import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from "@angular/router";
import {SettingsComponent} from "./settings.component";

@NgModule({
  declarations: [
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  exports: [SettingsComponent]
})
export class SettingsModule { }

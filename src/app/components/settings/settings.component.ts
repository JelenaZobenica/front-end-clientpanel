import { Component, OnInit } from '@angular/core';
import {SettingsService} from "../../services/settings.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {Settings} from "../../models/settings";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  settings: Settings;

  constructor(
    private settingsService: SettingsService,
    private flashMessagesService: FlashMessagesService
  ) { }

  ngOnInit() {
    this.settings = this.settingsService.getSettings();
  }

  onSubmit() {
    this.settingsService.changeSettings(this.settings);
    this.flashMessagesService.show('Settings saved', {cssClass: 'alert-success', timeout: 4000});
  }

}

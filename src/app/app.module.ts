import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClientDetailsComponent } from './components/client-details/client-details.component';
import { AddClientComponent } from './components/add-client/add-client.component';
import { EditClientComponent } from './components/edit-client/edit-client.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SettingsComponent } from './components/settings/settings.component';
import { RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabase, AngularFireDatabaseModule} from 'angularfire2/database';
import {ClientService} from './services/client.service';
import {DashboardModule} from './components/dashboard/dashboard.module';
import {AddClientModule} from "./components/add-client/add-client.module";
import {FlashMessagesModule} from "angular2-flash-messages";
import {ClientDetailsModule} from "./components/client-details/client-details.module";
import {EditClientModule} from "./components/edit-client/edit-client.module";
import {LoginModule} from "./components/login/login.module";
import {AuthService} from "./services/auth.service";
import {AuthGuard} from "./components/guards/auth.guard";
import {RegisterModule} from "./components/register/register.module";
import {SettingsService} from "./services/settings.service";
import {RegisterGuard} from "./components/guards/register.guard";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {NavbarModule} from "./components/navbar/navbar.module";
import {SettingsModule} from "./components/settings/settings.module";

const appRoutes: Routes = [
  {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [RegisterGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'add-client', component: AddClientComponent, canActivate: [AuthGuard]},
  {path: 'client/:id', component: ClientDetailsComponent, canActivate: [AuthGuard]},
  {path: 'edit-client/:id', component: EditClientComponent, canActivate: [AuthGuard]},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]},
  {path: '**', component: PageNotFoundComponent}
];

export const firebaseConfig = {
  apiKey: 'AIzaSyBNko_VSe-OSmCKJvYYjl8v-ZunahJtreo',
  authDomain: 'clientpanel-27f95.firebaseapp.com',
  databaseURL: 'https://clientpanel-27f95.firebaseio.com',
  storageBucket: 'clientpanel-27f95.appspot.com',
  messagingSenderId: '990385573943'
};

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    DashboardModule,
    AddClientModule,
    FlashMessagesModule,
    ClientDetailsModule,
    EditClientModule,
    LoginModule,
    RegisterModule,
    NavbarModule,
    SettingsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AngularFireAuth,
    AngularFireDatabase,
    ClientService,
    AuthService,
    AuthGuard,
    SettingsService,
    RegisterGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

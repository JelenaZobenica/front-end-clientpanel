import {Component, OnDestroy, OnInit} from '@angular/core';
import {ClientService} from "../../services/client.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FlashMessagesService} from "angular2-flash-messages";
import {Client} from "../../models/client";
import {Subject} from "rxjs/Subject";
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html'
})
export class ClientDetailsComponent implements OnInit, OnDestroy {

  id: string;
  client: Client;
  hasBalance = false;
  showBalanceUpdateInput = false;
  unsubscribe = new Subject<void>();

  constructor(
    private clientService: ClientService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.clientService.getClient(this.id)
      .valueChanges()
      .takeUntil(this.unsubscribe)
      .subscribe(client => {
        this.client = client;

        if(this.client.balance > 0) {
          this.hasBalance = true;
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  updateBalance(id: string) {
    this.clientService.updateClient(id, this.client);
    this.flashMessagesService.show('Balance updated', {cssClass: 'alert-success', timeout: 4000});
    this.router.navigate(['/client/' + id]);
  }

  deleteClient() {
    if(confirm("Are you sure to delete client")) {
      this.clientService.deleteClient(this.id);
      this.flashMessagesService.show('Client ' + this.client.firstName + ' ' + this.client.lastName + ' successfuly deleted',
        {cssClass: 'alert-success', timeout: 4000});
      this.router.navigate(['/']);
    }
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {DashboardComponent} from './dashboard.component';
import {ClientsModule} from '../clients/clients.module';
import {SidebarModule} from '../sidebar/sidebar.module';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ClientsModule,
    SidebarModule
  ],
  providers: [],
  exports: [DashboardComponent]
})
export class DashboardModule { }


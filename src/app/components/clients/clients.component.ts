import {Component, OnDestroy, OnInit} from '@angular/core';
import {Client} from '../../models/client';
import {ClientService} from '../../services/client.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html'
})
export class ClientsComponent implements OnInit, OnDestroy {

  clients: Client[];
  totalOwed: number;
  unsubscribe = new Subject<void>();

  constructor(private clientService: ClientService) {}

  ngOnInit() {
    this.clientService.getClients().takeUntil(this.unsubscribe).subscribe(clients => {
      this.clients = clients;
      this.getTotalOwed();
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  removeDuplicatesClients(): Array<Client> {
    const uniqueClients = new Array<Client>();
    for(let i = 0; i < this.clients.length; i++) {
      if(uniqueClients.indexOf(this.clients[i]) === -1) {
        uniqueClients.push(this.clients[i]);
      }
    }
    return uniqueClients;
    // this.clients = uniqueClients.filter(client => this.clients.indexOf(client) === -1);
  }

  private getTotalOwed() {
    const initialValue = 0;
    this.totalOwed = this.clients.reduce(((sum, value) => sum + parseFloat('' + value.balance)), initialValue);
  }
}

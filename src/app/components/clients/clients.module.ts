import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {ClientsComponent} from './clients.component';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    ClientsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  exports: [ClientsComponent]
})
export class ClientsModule { }

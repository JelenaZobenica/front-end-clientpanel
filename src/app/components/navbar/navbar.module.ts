import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule} from "@angular/router";
import {NavbarComponent} from "./navbar.component";

@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  exports: [NavbarComponent]
})
export class NavbarModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {ClientDetailsComponent} from "./client-details.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    ClientDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  exports: [ClientDetailsComponent]
})
export class ClientDetailsModule { }

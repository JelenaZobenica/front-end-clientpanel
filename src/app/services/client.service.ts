import { Observable } from 'rxjs/Observable';
import {Injectable, OnDestroy} from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Client } from '../models/Client';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/take';


@Injectable()
export class ClientService implements OnDestroy {
  private clientsRef: AngularFireList<Client[]>;
  private clients: Subject<Client[]>;
  private client: AngularFireObject<Client>;
  private unsubscribe = new Subject<void>();
  private toDashboard = new Subject<string>();

  constructor(private af: AngularFireDatabase) {
    this.clientsRef = this.af.list('clients');
  }

  ngOnDestroy() {
      this.unsubscribe.next();
      this.unsubscribe.complete();
  }

  getClients(): Observable<Client[]> {
    this.clients = new Subject<Client[]>();
    let clientObjFromFirebase: any;
    const clientList: Client[] = new Array<Client>();
    this.clientsRef.snapshotChanges().takeUntil(this.unsubscribe).subscribe(actions => {
      actions.forEach(action => {
        clientObjFromFirebase = action.payload.val();
        clientObjFromFirebase.key = action.key;
        clientList.push(clientObjFromFirebase);
        this.clients.next(clientList);
      });
    });
    return this.clients;
  }

  newClient(client: any) {
    this.clientsRef.push(client);
  }

  goToDashboardPage(go: string) {
    this.toDashboard.next(go);
    return this.toDashboard;
  }

  getClient(id: string) {
    this.client = this.af.object('/clients/'+id) as AngularFireObject<Client>;
    return this.client;
  }

  updateClient(id: string, client: any) {
    return this.clientsRef.update(id, client);
  }

  deleteClient(id: string) {
    return this.clientsRef.remove(id);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {AddClientComponent} from './add-client.component';

@NgModule({
  declarations: [
    AddClientComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  exports: [AddClientComponent]
})
export class AddClientModule { }
